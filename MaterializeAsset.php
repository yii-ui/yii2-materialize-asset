<?php

declare(strict_types=1);

namespace yiiui\yii2materialize;

use yii\web\AssetBundle;

/**
 * Class MaterializeAsset
 *
 * @package yiiui\yii2materialize
 */
class MaterializeAsset extends AssetBundle
{
    public $sourcePath = '@npm/materialize-css/dist';

    public $css = [
        'css/materialize.min.css'
    ];
}
