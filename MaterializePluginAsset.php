<?php

declare(strict_types=1);

namespace yiiui\yii2materialize;

use yii\web\AssetBundle;

/**
 * Class MaterializePluginAsset
 *
 * @package yiiui\yii2materialize
 */
class MaterializePluginAsset extends AssetBundle
{
    public $sourcePath = '@npm/materialize-css/dist';

    public $js = [
        'js/materialize.min.js'
    ];

    public $depends = [
        'yiiui\yii2materialize\MaterializeAsset'
    ];
}
