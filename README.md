Materialize Assets for Yii 2
============================

[![Latest Stable Version](https://img.shields.io/packagist/v/yii-ui/yii2-materialize-asset?label=stable&style=flat-square)](https://packagist.org/packages/yii-ui/yii2-materialize-asset)
[![Latest Version](https://img.shields.io/packagist/v/yii-ui/yii2-materialize-asset?include_prereleases&label=unstable&style=flat-square)](https://packagist.org/packages/yii-ui/yii2-materialize-asset)
[![PHP Version](https://img.shields.io/packagist/php-v/yii-ui/yii2-materialize-asset/dev-master?style=flat-square)](https://www.php.net/downloads.php)
[![License](https://img.shields.io/packagist/l/yii-ui/yii2-materialize-asset?style=flat-square)](https://packagist.org/packages/yii-ui/yii2-materialize-asset)
[![Total Downloads](https://img.shields.io/packagist/dt/yii-ui/yii2-materialize-asset?style=flat-square)](https://packagist.org/packages/yii-ui/yii2-materialize-asset)
[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat-square)](http://www.yiiframework.com/)


This is an [Yii framework 2.0](http://www.yiiframework.com) asset bundle for the [Materialize CSS Framework](http://materializecss.com/).


Installation
------------

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run
```
php composer.phar require yii-ui/yii2-materialize-asset
```
or add
```
"yii-ui/yii2-materialize-asset": "~1.0.0"
```
to the require section of your `composer.json` file.

Documentation
------------

[Documentation](https://www.yii-ui.com/) will be added soon at https://www.yii-ui.com/.

License
-------

**yii-ui/yii2-materialize-asset** is released under the MIT License. See the [LICENSE.md](LICENSE.md) for details.
