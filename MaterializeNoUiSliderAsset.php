<?php

declare(strict_types=1);

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class MaterializeNoUiSliderAsset
 *
 * @package frontend\assets
 */
class MaterializeNoUiSliderAsset extends AssetBundle
{
    public $sourcePath = '@npm/materialize-css/extras/noUiSlider';

    public $css = [
        'nouislider.css'
    ];

    public $js = [
        'nouislider.min.js'
    ];
}
